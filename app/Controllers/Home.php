<?php namespace App\Controllers;
use App\Models\Role;

class Home extends BaseController
{
	public function index()
	{
		$roleModel = new Role();
		
		//var_dump($roleModel->findAll());
		return view('home');
	}

	//--------------------------------------------------------------------

}
