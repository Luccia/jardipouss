<?php namespace App\Models;

use CodeIgniter\Model;

class Commerce extends Model
{
    protected $table = "Commerce";
    protected $primaryKey = "id";

    protected  $returnType = "object";
    protected $useSoftDeletes = false;
    protected $allowedFields = ["nom","telephone","User_id","Adresse_id"];

}