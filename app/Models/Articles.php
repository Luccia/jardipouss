<?php namespace App\Models;

use CodeIgniter\Model;

class Articles extends Model
{
    protected $table = "Articles";
    protected $primaryKey = "id";

    protected  $returnType = "object";
    protected $useSoftDeletes = false;
    protected $allowedFields = ["nom","description","image","Categorie_id"];

}