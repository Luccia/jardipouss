<?php namespace App\Models;

use CodeIgniter\Model;

class Publicite extends Model
{
    protected $table = "Publicite";
    protected $primaryKey = "id";

    protected  $returnType = "object";
    protected $useSoftDeletes = false;
    protected $allowedFields = ["image","lien","commerce_id"];

}