<?php namespace App\Models;

use CodeIgniter\Model;

class Role extends Model
{
    protected $table = "Role";
    protected $primaryKey = "id";

    protected  $returnType = "object";
    protected $useSoftDeletes = false;
    protected $allowedFields = ["roleName"];

}