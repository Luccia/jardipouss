<?php namespace App\Models;

use CodeIgniter\Model;

class UserHasJardin extends Model
{
    protected $table = "User_has_jardin";

    protected  $returnType = "object";
    protected $useSoftDeletes = false;
    protected $allowedFields = ["role_id"];

}