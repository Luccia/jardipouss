<?php namespace App\Models;

use CodeIgniter\Model;

class Adresse extends Model
{
    protected $table = "Adresse";
    protected $primaryKey = "id";

    protected  $returnType = "object";
    protected $useSoftDeletes = false;
    protected $allowedFields = ["lattitude","longitude","numero","voie","complement","codepostal","ville"];

}