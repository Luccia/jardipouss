<?php namespace App\Models;

use CodeIgniter\Model;

class User extends Model
{
    protected $table = "Role";
    protected $primaryKey = "id";

    protected  $returnType = "object";
    protected $useSoftDeletes = false;
    protected $allowedFields = ["nom","prenom","pseudo","avatar","presentation","motdepasse","titre","telephone","role_id"];

}