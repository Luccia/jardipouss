<?php namespace App\Models;

use CodeIgniter\Model;

class Categorie extends Model
{
    protected $table = "Categorie";
    protected $primaryKey = "id";

    protected  $returnType = "object";
    protected $useSoftDeletes = false;
    protected $allowedFields = ["nom","icone"];

}